#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Config::Simple;
use Getopt::Long qw( :config no_ignore_case auto_help auto_version );
use Pod::Usage;
use Locale::Codes::Country;
use Geo::IP;

my $config = '/etc/fail2stats/db.ini';

my ( $help, $man, $usage, $verbose );

if ( @ARGV > 0 ) {
	GetOptions (
    'c|config=s'      => \$config,
		'v|verbose'       => \$verbose,
		'h|help'          => \$help,
		'u|usage'         => \$usage,
		'man'             => \$man,
	) or pod2usage( { -exitstatus => 1, -verbose => 1 } );
}

pod2usage( { -exitstatus => 0, -verbose => 0 } ) if $usage;
pod2usage( { -exitstatus => 0, -verbose => 1 } ) if $help;
pod2usage( { -exitstatus => 0, -verbose => 2 } ) if $man;
$verbose = 1;
if( $verbose ) {
  print "Config file: $config\n";
}

my ($ipaddr, $jail) = @ARGV;
my $gi = Geo::IP->open('/usr/share/GeoIP/GeoIP.dat', GEOIP_STANDARD);
my $country = $gi->country_code_by_addr($ipaddr);
my $country3 = uc(country_code2code($country,LOCALE_CODE_ALPHA_2,LOCALE_CODE_ALPHA_3));

if( $verbose ) {
  print "Addr:       $ipaddr\n";
  print "Country a2: $country\n";
  print "Country a3: $country3\n";
}

my $db = {};
Config::Simple->import_from($config, $db);

my $dbh;
my $dsn = "DBI:$db->{'db.driver'}:dbname=$db->{'db.base'}";
if ( $db->{'db.driver'} eq 'mysql' ) {
  $dsn .= ";host=$db->{'db.host'}";
  $dsn .= ";mysql_socket=$db->{'db.socket'}" if ( defined( $db->{'db.socket'} ) );
}
print "dsn: $dsn\n" if( $verbose );

if ( defined( $db->{'db.password'} ) ) {
  $dbh = DBI->connect( $dsn, "$db->{'db.user'}", "$db->{'db.password'}" ) or 
    die( "Could not connect to database: $dbI::errstr" );
}
else {
  $dbh = DBI->connect( $dsn, "$db->{'db.user'}" ) or 
    die( "Could not connect to database: $dbI::errstr" );
}

my $query = "INSERT INTO ban(address,jail,country,country3) VALUES('$ipaddr','$jail','$country','$country3')";
my $sth = $dbh->prepare($query);
$sth->execute() or die( "MySQL query returns an error: $dbI::errstr. Query was: $query" );

__END__

=head1 NAME

fail2stats - Save data from fail2ban in database

=head1 SYNOPSIS

fail2stats.pl [options] [ip-addr] [jail]

Save ip address and jail name from fail2ban in database.

Application exit with a 0 code in success and no zero value in failure.

Please restart with '-h' or '--help' to have more information about availables
options

=head1 OPTIONS

=over 8

=item -c, --config <FILE>
Use these config file instead of default one: /etc/fail2stats/db.ini.

=item -h, --help
Show help and exit.

=item -u, --usage
Show usage and exit

=item -v, --verbose
Make application more talkative

=item --man
Prints the manual page and exits.

=back

=head1 DESCRIPTION

B<fail2stats> stores ban data into database.

=head1 COPYRIGHT, LICENSE AND WARRANTY

This program is copyright 2011-2015 B2PWeb.
Feedback and improvements are welcome.

THIS PROGRAM IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, version 2; OR the Perl Artistic License.  On UNIX and similar
systems, you can issue `man perlgpl' or `man perlartistic' to read these
licenses.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place, Suite 330, Boston, MA  02111-1307  USA.

=head1 AUTHOR

Didier Fabert (TartareFR) <didier@tartarefr.eu>

=cut
