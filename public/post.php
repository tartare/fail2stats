<?php

  function exitError( $code, $message )
  {
      header( "HTTP/1.1 500 Internal Server Error" );
      header( 'Content-Type: application/json; charset=UTF-8' );
      die( json_encode( array( 'message' => $message, 'code' => $code ) ) . PHP_EOL );
  }
  function exitSuccess( $message )
  {
      header( "HTTP/1.1 200 OK" );
      //header( 'Content-Type: application/json; charset=UTF-8' );
      echo json_encode( $message );
      exit( 0 );
  }
  
  function getData()
  {
    $answer = array();
    $configfile = '/home/didier/vcs/fail2stats/conf/db.ini';
    if(! is_readable($configfile) ) {
      exitError( 1, "Error: cannot read configuration file" );
    }
    $conf = parse_ini_file($configfile, true);

    $dsn = strtolower($conf['db']['driver']).':';
    if ( strtolower($conf['db']['driver']) == 'sqlite' ) {
      $dsn .= $conf['db']['base'];
    }
    elseif ( $conf['db']['driver'] == 'mysql' ) {
      $dsn .= 'dbname='.$conf['db']['base'].';host='.$conf['db']['host'];
      if ( defined( $conf['db']['socket'] ) ) $dsn .= ';mysql_socket='.$conf['db']['socket'];
    }

    $queries = array(
      'countries' => "SELECT DISTINCT country,country3,COUNT(*) AS nb FROM ban GROUP BY country3 ORDER BY nb DESC",
      'jails' => "SELECT DISTINCT jail,COUNT(*) AS nb FROM ban GROUP BY jail ORDER BY nb DESC",
    );
    try {
      $dbh = new PDO( $dsn, ( isset( $conf['db']['user'] ) ) ? $conf['db']['user'] : null, ( isset( $conf['db']['password'] ) ) ? $conf['db']['password'] : null );
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      foreach( $queries as $key => $query ) {
        foreach ($dbh->query($query) as $row) {
          if($key == "countries" ) {
            $answer[$key][]  = [ $row['country'], $row['country3'], (int)$row['nb'] ];
          }
          elseif($key == "jails" ) {
            $answer[$key][]  = [ $row['jail'], (int)$row['nb'] ];
          }
        }
      }
    }
    catch (PDOException $e) {
      exitError( 2, $e->getMessage() . '. DSN: ' . $dsn );
    }
    exitSuccess($answer);
  }
  
  function getTranslations()
  {
  }

  $query = 'data';
  if ( isset( $_POST['query'] ) and ! empty( $_POST['query'] ) ) {
    $query = urldecode( $_POST['query'] );
  }
  if( $query == 'data' ) {
    getData();
  }
