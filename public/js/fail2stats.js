var map;

$(document).ready( function() {
  getData();
});


function  getData() {
  $.ajax({
    type: 'POST',
    url: "post.php",
    data: {
      "instance": encodeURI('dummy'),
    },
    success: function( json ) {
      var data = JSON.parse( json );
      paintMap(data.countries);
      fillCountryData(data.countries);
      getCountryChart(data.countries);
      getJailChart(data.jails);
    },
    error: function( jqXHR ) {
      var message;
      if(jqXHR.responseJSON) {
        message = 'Error ' + jqXHR.responseJSON.code + ': ' + jqXHR.responseJSON.message;
      }
      else {
        message = 'Error ' + jqXHR.response;
      }
      $('#worldmap').html(message);
      $('#charts').html(message);
      $('#datalist').html(message);
    },
  });
}

function getCountryChart(countries)
{
    var labellist = [];
    var valuelist = [];
    countries.forEach(function(item){
        labellist.push(item[1]);
        valuelist.push(item[2]);
    });
    var ctx = document.getElementById('jailchart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnut',

        // The data for our dataset
        data: {
            labels: labellist,
            datasets: [{
                backgroundColor: [ '#aa0000', '#00aaff', '#00aa00', '#e8e873', '#e79a00', 'lightgrey', '#aaaaff' ],
                borderColor: 'black', //'rgb(255, 99, 132)',
                data: valuelist,
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false,
                position: 'right',
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: 'white',
                    fontSize: 20
                },
            },
            title: {
                display: true,
                position: 'top',
                fontColor: 'white',
                fontSize: 24,
                text: 'Ban by country'
            },
            tooltips: {
                bodyFontSize: 18,
                bodyFontColor: 'white'          
            }
        }
    });
}

function getJailChart(jails)
{
    var labellist = [];
    var valuelist = [];
    jails.forEach(function(item){
        labellist.push(item[0]);
        valuelist.push(item[1]);
    });
    var ctx = document.getElementById('countrychart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnut',

        // The data for our dataset
        data: {
            labels: labellist,
            datasets: [{
                backgroundColor: [ '#aa0000', '#00aaff', '#00aa00', '#e8e873', '#e79a00', 'lightgrey', '#aaaaff' ],
                borderColor: 'black', //'rgb(255, 99, 132)',
                data: valuelist,
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: true,
                position: 'right',
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: 'white',
                    fontSize: 20
                },
            },
            title: {
                display: true,
                position: 'top',
                fontColor: 'white',
                fontSize: 24,
                text: 'Ban by jail'
            },
            tooltips: {
                bodyFontSize: 18,
                bodyFontColor: 'white'          
            }
        }
    });
}

function fillCountryData(countries)
{
    var html = '<div>';
    countries.forEach(function(item) {
        html += '<div class="country-data-item">' + 
                '<span class="flag-icon flag-icon-' + item[0].toLowerCase()  + '"></span>&nbsp;' + item[1] + '&nbsp;:' + item[2]  + '' +
                '</div>';
    });
    html += '</div>';
    $('#datalist').html(html);
}

function paintMap(countries)
{
    // Datamaps expect data in format:
    // { "USA": { "fillColor": "#42a844", numberOfWhatever: 75},
    //   "FRA": { "fillColor": "#8dc386", numberOfWhatever: 43 } }
    var dataset = {};
    // We need to colorize every country based on "numberOfWhatever"
    // colors should be uniq for every value.
    // For this purpose we create palette(using min/max countries-value)
    var onlyValues = countries.map(function(obj){ return obj[2]; });
    var minValue = Math.min.apply(null, onlyValues),
            maxValue = Math.max.apply(null, onlyValues);
    // create color palette function
    // color can be whatever you wish
    var paletteScale = d3.scale.linear()
            .domain([minValue,maxValue])
            .range(["#c9c9d9","#5555ff"]); // blue color
            //.range(["#ffcc00","#ff0000"]); // blue color
    // fill dataset in appropriate format
    countries.forEach(function(item){ //
        // item example value ["USA", 70]
        var iso2 = item[0],
            iso3 = item[1],
            value = item[2];
        dataset[iso3] = {
          numberOfThings: value,
          fillColor: paletteScale(value),
          isoCode2: iso2.toLowerCase()
        };
    });
    // render map
    new Datamap({
        element: document.getElementById('worldmap'),
        projection: 'mercator', // big world map
        // countries don't listed in dataset will be painted with this color
        fills: { defaultFill: '#D0D0D0' },
        data: dataset,
        geographyConfig: {
            borderColor: '#313131',
            highlightBorderWidth: 2,
            // don't change color on mouse hover
            highlightFillColor: function(geo) {
                return geo['fillColor'] || '#F9F9F9';
            },
            // only change border
            highlightBorderColor: '#ffffff',
            // show desired information in tooltip
            popupTemplate: function(geo, data) {
              console.debug(geo);
                // don't show tooltip if country don't present in dataset
                if (!data) { 
                  return ['<div class="hoverinfo"><span class="flag-icon flag-icon-', geo.id.toLowerCase(), '"></span>',
                    '&nbsp;<strong>', geo.id, '</strong>',
                    '<br><strong>', geo.properties.name, '</strong>',
                    '<br>No Ban', '</div>'].join('');
                }
                // tooltip content
                return ['<div class="hoverinfo"><span class="flag-icon flag-icon-', geo.id.toLowerCase(), '"></span>',
                    '&nbsp;<strong>', geo.id, '</strong>',
                    '<br><strong>', geo.properties.name, '</strong>',
                    '<br>Ban: <strong>', data.numberOfThings, '</strong>',
                    '</div>'].join('');
            }
        }
    });
};
