CREATE TABLE IF NOT EXISTS `ban` (
  `id` int(11) NOT NULL,
  `address` varchar(15) NOT NULL,
  `jail` varchar(32) NOT NULL,
  `country` varchar(2) NOT NULL,
  `country3` varchar(3) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ban`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
