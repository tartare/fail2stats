# fail2stats

Make stats from fail2ban:
* interactive map
* graph about country and jail statistics

## Prerequisites

### With SQLite

1. Create a database with ban table

    ```shell
    sqlite3 /var/lib/fail2stats/fail2stats.db
    ```
    ```sql
    CREATE TABLE IF NOT EXISTS ban( \
        id INTEGER PRIMARY KEY AUTOINCREMENT, \
        address varchar(15), \
        jail varchar(32), \
        country varchar(2), \
        country3 varchar(3), \
        createdate timestamp DEFAULT CURRENT_TIMESTAMP \
    );
    ```

### MySQL

1. Create a MySQL database and a dedicated user

    ```sql
    mysql -u root
    
    CREATE DATABASE IF NOT EXISTS fail2stats ;
    
    CREATE USER 'fail2stats'@'localhost' IDENTIFIED BY 'mysuperpasswd';
    
    GRANT USAGE ON *.* TO 'fail2stats'@'localhost' IDENTIFIED BY 'mysuperpasswd' WITH MAX_QUERIES_PER_HOUR 0 \
    MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
    
    GRANT ALL PRIVILEGES ON fail2stats.* TO 'fail2stats'@'localhost';
    ```
    
2. Create the ban table

    ```sql
    mysql fail2stats < sql/db.sql
    ```

## Install
    
1. Copy fail2stats.pl to /usr/local/bin and make sure it is executable

    ```shell
    cp fail2stats.pl /usr/local/bin
    chmod +x /usr/local/bin/fail2stats.pl
    ```
    
2. Create config dir, copy and modify db.ini with your database settings

    ```shell
    mkdir /etc/fail2stats
    cp conf/db.ini /etc/fail2stats/
    ```
    
3. Add fail2stats.conf to fail2ban actions

    ```shell
    cp fail2ban/fail2stats.conf /etc/fail2ban/action.d/
    ```

4. Modify /etc/fail2ban/jail.local to add the new fail2stats action

    ```shell
    action = %(action_mwl)s
             fail2stats[name=%(__name__)s]
    ```

5. Copy all public files to apache www dir

    ```shell
    mkdir /var/www/fail2stats
    cp -ra public/* /var/www/fail2stats
    ```

6. Add apache conf

    ```shell
    cp conf/fail2stats.conf /etc/httpd/conf.d/
    ```
    
7. If you use SELinux, restore context to new files

    For SQLite only
    
    ```shell
    semanage fcontext -a -t fail2ban_var_lib_t '/var/lib/fail2stats(/.*)?'
    restorecon -R -v /var/lib/fail2stats
    ```
    
    For all
    
    ```shell
    restorecon -Rv /usr/local/bin/fail2stats.pl \
    /etc/fail2stats /etc/fail2ban/action.d/fail2stats.conf \
    /var/www/fail2stats /etc/httpd/conf.d/fail2stats.conf
    ```

8. Restart services

    ```shell
    systemctl restart fail2ban httpd
    ```
    
## Webpage

![Screenshot](screenshot.png "Screenshot")

## External ressources

* A [modified version](https://github.com/didier13150/flag-icon-css) of [flag-icon-css](https://github.com/lipis/flag-icon-css), which add ISO 3166-1 alpha3 country code.
* [datamap](http://datamaps.github.io/)
* [chartjs](https://www.chartjs.org)
